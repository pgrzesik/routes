### Find routes solution

Solution for find routes is contained in `src/find_routes.py`. Repository also contains `format_routes.py`, which has helper methods for formatting routes.

To run tests:
1. Create and activate virtualenv
2. Run `pip install -r requirements.dev.txt`
3. Run `python -m pytest --flake8`

To check example usage:
1. Run `python example.py`
