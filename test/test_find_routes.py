from src.find_routes import find_routes


def test_does_not_find_routes_for_nonexistent_origin():
    origin = 'nonexistent'
    destination = 'nonexistent'

    expected_result = []

    result = find_routes(origin, destination)

    assert result == expected_result


def test_does_not_find_routes_for_nonexistent_destination():
    origin = 'Seattle'
    destination = 'nonexistent'

    expected_result = []

    result = find_routes(origin, destination)

    assert result == expected_result


def test_finds_route_for_the_same_destination_as_origin():
    origin = 'Seattle'

    expected_result = [['Seattle']]

    result = find_routes(origin, origin)

    assert result == expected_result


def test_finds_direct_route():
    origin = 'Seattle'
    destination = 'LA'
    expected_result = [['Seattle', 'LA']]

    result = find_routes(origin, destination)

    assert result == expected_result


def test_finds_routes_with_multiple_steps():
    origin = 'Seattle'
    destination = 'Orlando'

    expected_result = [
        ['Seattle', 'LA', 'Orlando'],
        ['Seattle', 'LA', 'Portland', 'Orlando'],
        ['Seattle', 'Orlando'],
    ]

    result = find_routes(origin, destination)

    assert result == expected_result
