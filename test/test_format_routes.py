from src.format_routes import format_route, format_routes


class TestFormatRoute:
    def test_empty(self):
        route = []
        expected_result = ''

        result = format_route(route)

        assert result == expected_result

    def test_with_one_step(self):
        route = ['a']
        expected_result = 'a'

        result = format_route(route)

        assert result == expected_result

    def test_with_multiple_steps(self):
        route = ['a', 'b', 'c']
        expected_result = 'a -> b -> c'

        result = format_route(route)

        assert result == expected_result


class TestFormatRoutes:
    def test_empty(self):
        routes = []
        expected_result = []

        result = format_routes(routes)

        assert result == expected_result

    def test_with_one_route(self):
        routes = [['Seattle', 'LA']]
        expected_result = ['Seattle -> LA']

        result = format_routes(routes)

        assert result == expected_result

    def test_with_multiple_routes(self):
        routes = [['Seattle', 'LA'], ['Seattle', 'Orlando', 'LA']]
        expected_result = ['Seattle -> LA', 'Seattle -> Orlando -> LA']

        result = format_routes(routes)

        assert result == expected_result
