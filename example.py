from src.find_routes import find_routes
from src.format_routes import format_routes

if __name__ == '__main__':
    origin = 'Seattle'
    destination = 'Orlando'
    result = format_routes(find_routes('Seattle', 'Orlando'))
    print(result)
