from .get_flights import get_flights

flights = get_flights()


def find_routes(origin, destination, starting_route=[]):
    direct_destinations = flights.get(origin, None)

    if direct_destinations is None:
        return []

    current_route = [*starting_route, origin]

    if origin == destination:
        return [current_route]

    routes = []

    for direct_destination in direct_destinations:
        if direct_destination not in current_route:
            routes.extend(
                find_routes(
                    direct_destination, destination, current_route
                )
            )

    return routes
