def format_route(route):
    if not route:
        return ''

    return ' -> '.join(route)


def format_routes(routes):
    return [format_route(route) for route in routes]
