FLIGHTS = {
    'Seattle': ['LA', 'Orlando'],
    'LA': ['Orlando', 'Maine', 'Portland'],
    'Orlando': ['Seattle'],
    'Portland': ['Orlando']
}


def get_flights():
    return FLIGHTS
